from pizza import app


@app.route('/')
@app.route('/index')
def index():
    return 'Hello! Welcome to RavenPack Pizza!'
